import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import firebase from "firebase/app";
import "firebase/auth";
import "./plugins/vee-validate";
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

var firebaseConfig = {
  apiKey: "AIzaSyCJi9hWLzyWYXU1NIkCTRf1sGqFvNLUP9Y",
  authDomain: "ex-crowd.firebaseapp.com",
  projectId: "ex-crowd",
};
Vue.config.productionTip = false
firebase.initializeApp(firebaseConfig);
router.beforeResolve((to, from, next) => {
  const isPublic = to.matched.some((record) => record.meta.isPublic);
  if (isPublic) {
    document.title = to.meta.title + " | サンプル";
    next();
    return;
  }
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      next();
      return;
    }
    //ログインしてない
    next({
      path: "/signin",
    });
  });
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')