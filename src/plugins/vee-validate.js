import Vue from 'vue';
import {
    extend,
    ValidationObserver,
    ValidationProvider
} from "vee-validate";
import * as rules from "vee-validate/dist/rules";
import {
    messages
} from "vee-validate/dist/locale/ja.json";

Object.keys(rules).forEach(rule => {
    extend(rule, {
        ...rules[rule], // copies rule configuration
        message: messages[rule] // assign message
    });
});
extend('num_hyphen', {
    message: '{_field_}は半角数字、ハイフンのみ使用できます。',
    // eslint-disable-next-line no-useless-escape
    validate: (value) => value.match(/^[0-9\-]+$/)
});
extend('tel', {
    message: '{_field_}は日本の電話番号のみ入力お願いします。',
    validate: (value) => value.match(/^0\d{2,3}-\d{1,4}-\d{4}$/) || value.match(/^0\d{9,10}$/)
});
extend('zen_kana', {
    message: '{_field_}は全角ひらがなのみ使用できます。',
    // eslint-disable-next-line no-irregular-whitespace
    validate: (value) => value.match(/^[ぁ-んー　]*$/)
});
extend('zen_katakana', {
    message: '{_field_}は全角カタカナのみ使用できます。',
    // eslint-disable-next-line no-irregular-whitespace
    validate: (value) => value.match(/^[ァ-ヶー　]*$/)
});
extend('zip_search', {
    params: ["history"],
    message: '郵便番号の入力または変更を行った場合は、検索を押してください。',
    validate: (value, {
        history
    }) => value === history
});
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);