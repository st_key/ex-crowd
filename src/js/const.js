export default Object.freeze({
    url: (location.hostname === "localhost" ? "http://" : "https://").toString() + location.hostname + "/",
    storage: "hogefoo"
});